package com.example.lilith;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button boton1;
    private Button boton2;
    private Button boton3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boton1 = (Button) findViewById(R.id.btn_reg);
        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarActivity1(v);
            }
        });
        boton2 = (Button) findViewById(R.id.rec_pass);
        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarActivity2(v);
            }
        });
        boton3 = (Button) findViewById(R.id.entrar);
        boton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarActivity3(v);
            }
        });
    }

    public void lanzarActivity1(View v) {
        Intent i = new Intent(this, registrar.class);
        startActivity(i);
    }

    public void lanzarActivity2(View v) {
        Intent i = new Intent(this, recordar_pass.class);
        startActivity(i);
    }
    public void lanzarActivity3(View v) {
        /*Intent i = new Intent(this, recordar_pass.class);
        startActivity(i);*/
    }

}